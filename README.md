% CFML 2.0 TUTORIAL
% Arthur Charguéraud
% License : GNU GPL v3.

Description
===========

Use Coq 8.6 or Coq 8.7.

Compile with:
   make

(or make -j 4)

Then open:
   coqide -R . TUTO JFLA.v

(or use proof general, but it needs to be reasonably recent)



% git clone git@gitlab.inria.fr:charguer/cfml_tuto.git
% git clone https://gitlab.inria.fr/charguer/cfml_tuto.git