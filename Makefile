##############################################################################
# Certain custom settings can be defined in settings.sh.

# To specify the location of the Coq binaries, define COQBIN (with a
# trailing slash), e.g. COQBIN=/var/tmp/coq/bin/.
# If COQBIN is undefined, then "coqc" is used.

SERIOUS := 1
-include settings.sh

COQFLAGS:= -w -notation-overridden,-implicits-in-term


##############################################################################

COQINCLUDE := -R $(shell pwd) TUTO


export COQINCLUDE
export SERIOUS
export COQBIN
export COQFLAGS


.PHONY: all clean

all: _CoqProject

all depend clean _CoqProject:
	$(MAKE) -f Makefile.coq $@

